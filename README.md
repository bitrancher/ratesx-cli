rate.sx cli
===========
Just a wrapper for [rate.sx](https://rate.sx).

Why
---
Because I wanted to type `rx btc` instead of `curl -s rate.sx/btc`.

And then I wanted it to have shell style syntax, like `rx -e eth btc`.

And then I wanted it to play nicely with other shell scripts, like
```bash
rx $(electrum -w ~/.electrum/wallets/some_wallet getbalance | jq ".confirmed" | sed "s/\"//g")btc
```

And then I wanted a proper currency icon instead of always "$" when asking for a non-USD exchange rate.

Installation
------------

- ensure you have cURL
- ensure you are using a good font in your terminal
- put ratesx.sh somewhere in your $PATH
- rename it how you like

Usage
-----
```
ratesx.sh -H
```

Contributing
------------
Feel free to PR:

- currency characters/icons (must be available in nerdfonts/other popular CLI font packs)
- bug fixes
- simplifications
- support for new rate.sx features

Other features, complications, elaborations, and general bloat will likely be rejected.

License
-------
[Unlicense](https://unlicense.org)

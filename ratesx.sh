#!/bin/bash
no_color=false
quiet=false
number=false
exchange=false
verbose=false
query=""
symbol=""
char="$"
declare -A chars

chars["btc"]="₿"
chars["BTC"]="₿"
chars["GBP"]="£"
chars["gbp"]="£"
chars["ETH"]="ﲹ"
chars["eth"]="ﲹ"

usage="usage: $0 [-cChHqvs] [-n <count>] [-e <currency>] [<symbol>[@<time period>]]";

while getopts ":ce:hHn:stqv" option; do
  case $option in
    c) curl -s "rate.sx/:currencies"; exit ;;
    e) exchange=$OPTARG ;;
    h) echo $usage; exit ;;
    H) echo $usage
       echo ""
       echo "  -c             list currencies and exit (see -e)"
       echo "  -h             help"
       echo "  -H             verbose help"
       echo "  -q             quiet (don't show header & footer)"
       echo "  -v             verbose (for debugging)"
       echo "  -n <number>    number of results in exchange rate table (when no symbol specified)"
       echo "  -e <currency>  exchange currency: USD, CAD, etc. or <symbol>"
       echo "  -s             list symbols and exit (see <symbol>)"
       echo "  -t             plaintext (disable ANSI color)"
       echo "  <symbol>       BTC, ETH, etc."
       echo "  <time period>  a time period, used with <symbol> - 1d, 1m, 10y, etc."
       exit ;;
    n) number=$OPTARG ;;
    q) quiet=true ;;
    v) verbose=true ;;
    s) curl -s "rate.sx/:coins"; exit ;;
    t) no_color=true ;;
    ?) echo $usage; exit ;;
  esac
done

# remove the options from the positional parameters
shift $(( OPTIND - 1 ))

if [ $no_color ] || [ $quiet ] || [ $number ]; then
  query="${query}?"
fi

if [ "$number" != false ]; then
  query="$query&n=$number"
fi

if [ "$exchange" != false ]; then
  currency="${exchange}."
fi

if [ -n $1 ]; then
  symbol=$1
  shift 1
fi

if [ "$quiet" = true ]; then
  query="$query&q"
fi

if [ "$no_color" = true ]; then
  query="$query&T"
fi

if [ "$exchange" != false ] && [ "$no_color" = false ]; then
  if [ -v chars[$exchange] ]; then
    char="${chars[$exchange]}"
  fi
fi

url="query: ${currency}rate.sx/$symbol$query"

if [ "$verbose" = true ]; then
  echo "verbose: $verbose"
  echo "quiet: $quiet"
  echo "exchange: $exchange"
  echo "no color: $no_color"
  echo "number: $number"
  echo "symbol: $symbol"
  echo  "char: $char"
  echo $url
fi
curl -s $url | sed "s/\\$/$char/g"
